function chan = DTchan()

% chan = DTchan()
%   Return default DTchan structure:
%       chan(1).name = 'MemoryUsed';
%       chan(1).time = 0;
%       chan(1).data = 0;
%       chan(1).type = 'int64';
%       chan(1).mime = 'binary';
%       chan(1).meta = 'null';

chan.name = 'MemoryUsed';
chan.time = 0;
chan.data = 0;
chan.type = 'int64';
chan.mime = 'binary';
chan.meta = 'null';

end

%Copyright 2013 Cycronix

%Licensed under the Apache License, Version 2.0 (the "License"); 
%you may not use this file except in compliance with the License. 
%You may obtain a copy of the License at 

%http://www.apache.org/licenses/LICENSE-2.0 

%Unless required by applicable law or agreed to in writing, software 
%distributed under the License is distributed on an "AS IS" BASIS, 
%WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
%See the License for the specific language governing permissions and 
%limitations under the License.

