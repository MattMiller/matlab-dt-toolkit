function [ DTgot, nchan ] = DTnext( DT, delay, timeout )

%[ DTgot nchan ] = DTnext( DT [, delay, timeout] )
%  Get 'next' data after previous fetch without overlap.
%  Initialize with DT.reference 'newest', 'oldest', or 'absolute',
%  after that call with DT = DTprevious.
%
%  Parameters:
%  DT - DT structure defining what to get
%  delay - polling interval to check for new data
%  timeout - give up waiting after this long

if(nargin < 2) delay = 1;             % default poll once per sec
end
if(nargin < 3) timeout = 1000*delay;  % default long time but not forever
end

duration = DT.duration;             % remember this
smidge = 0.0001;                    % no overlap

if(~strcmp(DT.reference,'next'))   % initialize
    [DTgot, nchan] = DTget(DT);
    DTgot.reference = 'next';
    DTgot.start = DTgot.start + DTgot.duration + smidge;
    DTgot.duration = duration;      % regardless
    return;
end

imax = timeout / delay;
dget = DT;

for i=1:imax
    [dget, nchan] = DTget(dget);
    if(nchan)    % got something new
%        [dget.chan.time dget.chan.data ]
        DTgot = dget;
        DTgot.start = dget.start + dget.duration + smidge;
        DTgot.duration=duration;            % regardless
        return;
    end
    pause(delay);
end

DTgot = DT;      % notta
nchan = 0;
return;

%Copyright 2013 Cycronix

%Licensed under the Apache License, Version 2.0 (the "License"); 
%you may not use this file except in compliance with the License. 
%You may obtain a copy of the License at 

%http://www.apache.org/licenses/LICENSE-2.0 

%Unless required by applicable law or agreed to in writing, software 
%distributed under the License is distributed on an "AS IS" BASIS, 
%WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
%See the License for the specific language governing permissions and 
%limitations under the License.


